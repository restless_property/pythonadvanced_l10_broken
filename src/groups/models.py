"""
The module is a realization for the table of groups in LMS.
"""

from django.db import models


# Create your models here.
class Group(models.Model):
    """
    The class contains class attributes only
    and defies features of a group.
    """

    group_name = models.CharField(max_length=90, null=False)
    subject_name = models.CharField(max_length=90, null=False)
    lecturer = models.CharField(max_length=90, null=False)
    mentor = models.CharField(max_length=90, null=False)
    number_of_students = models.IntegerField(null=False)
    average_score = models.IntegerField(null=True)
