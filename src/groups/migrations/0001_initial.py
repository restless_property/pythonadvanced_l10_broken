# Generated by Django 3.1.4 on 2020-12-15 23:12

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('group_name', models.CharField(max_length=90)),
                ('subject_name', models.CharField(max_length=90)),
                ('lecturer', models.CharField(max_length=90)),
                ('mentor', models.CharField(max_length=90)),
                ('number_of_students', models.IntegerField()),
                ('average_score', models.IntegerField(null=True)),
            ],
        ),
    ]
