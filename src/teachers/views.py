from django.core.exceptions import ValidationError
from django.http import HttpResponse
from django.shortcuts import render  # noqa

from teachers.models import Teacher


# Create your views here.
from teachers.tools import format_table, repack_query_set # noqa


def get_teachers(request):
    teachers = Teacher.objects.all()
    params = ['first_name',
              'first_name__startswith',
              'first_name__endswith',
              'last_name',
              'last_name__startswith',
              'last_name__endswith',
              'age',
              'age__gt',
              'age__gte',
              'age__lt',
              'age__lte',
              'date_of_birth',
              'date_of_birth__startswith',
              'date_of_birth__endswith',
              'e_mail',
              'e_mail__startswith',
              'e_mail__endswith',
              'education',
              'education__startswith',
              'education__endswith',
              'exp_years',
              'exp_year__gt',
              'exp_years__gte',
              'exp_years__lt',
              'exp_years__lte',
              'groups',
              'groups__startswith',
              'groups__endswith'
              ]

    for param_name in params:
        param_value = request.GET.get(param_name)
        if param_value:
            try:
                teachers = teachers.filter(**{param_name: param_value})
            except ValidationError:
                return HttpResponse("Wrong date format "
                                    "for the parameter 'date_of_birth'. "
                                    "Must be 'YYYY-MM-DD'", status=400)
        data = repack_query_set(teachers)
    headers = ["id", "first_name", "last_name", "age", "date_of_birth",
               "e_mail", "education", "exp_years", "groups"]
    return render(request, "teachers/base.html", {"headings": headers, "data": data})
