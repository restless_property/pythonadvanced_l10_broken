from django.core.management.base import BaseCommand, CommandError
from polls.models import Poll


class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        pass