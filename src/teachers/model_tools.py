import random


def generate_teacher(faked):
    first_name = faked.first_name()
    last_name = faked.last_name()
    age = random.randint(21, 60)
    date_of_birth = faked.date_of_birth(minimum_age=age, maximum_age=age)
    mail_first = first_name.lower()
    mail_last = last_name.lower()
    e_mail = random.choice([f"{mail_first}_{mail_last}",
                            f"{mail_first}-{mail_last}",
                            f"{mail_first}{mail_last}",
                            f"{mail_first}{random.randint(111, 10000)}{mail_last}",
                            f'{mail_first}{random.randint(111, 10000)}',
                            f'{mail_first}',
                            f'{mail_first[0]}_{mail_last}',
                            f'{mail_last}_{mail_first}',
                            f'{mail_last}-{mail_first}'
                            ]) + '@' + faked.free_email_domain()
    specializations = {"Java Developer": ['Introduction Java', 'Java Elementary', 'Java Enterprise'],
                       "Web Developer": ['Front-End Basic', 'Front-End Pro', 'React'],
                       "PHP Developer": ['PHP', 'Basic PHP'],
                       "Python Developer": ['Introduction Python', 'Advanced Python'],
                       "DevOps Engineer": ['DevOps'],
                       "Data Scientist": ['Machine Learning'],
                       "QA Specialist": ['QA', 'QA Automation'],
                       "IT Manager": ['Project Management', 'HR IT', 'Business Analysis'],
                       "IT Designer": ['Design', 'UI/UX Design']}
    education = random.choice(list(specializations))
    exp_years = age - random.randint(18, age)
    groups = random.choices(specializations[education],
                            k=random.randint(1, 3))
    teacher = {'first_name': first_name,
               'last_name': last_name,
               'age': age,
               'date_of_birth': date_of_birth,
               'e_mail': e_mail,
               'education': education,
               'exp_years': exp_years,
               'groups': groups}
    return teacher
