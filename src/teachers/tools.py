
def format_table(records):
    """
    Formats the content of a 'Python' collection.

    :param collection: (collection) a 'Python' collection
    :return response: (str) a formatted content of the collection
    """
    response = ""
    for line in records:
        response += "<br>" + str(line)
    return response if response else '&ltNo results for your query&gt'


def repack_query_set(qs):
    qs_values = qs.values()
    dict_list = list(qs_values)
    data = [voc.values() for voc in dict_list]
    return data