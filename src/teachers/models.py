"""
The module is a realization for lecturers of groups in LMS.
"""

from django.db import models
from faker import Faker
#
from teachers.model_tools import generate_teacher


class Teacher(models.Model):
    """
    The class contains class attributes only
    and covers personal information on the lecturer of a group.

    Methods:
    generate_teachers: generates a range of fake lecturers' profiles
    """

    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=90, null=False)
    age = models.PositiveIntegerField(null=False)
    date_of_birth = models.DateField(null=True)
    e_mail = models.EmailField(null=True)
    education = models.TextField(null=True)
    exp_years = models.PositiveIntegerField(null=True)
    groups = models.TextField(null=False)

    def __str__(self):
        return f'Name: {self.first_name} {self.last_name}, ' \
               f'Age: {self.age}, ' \
               f'DoB: {self.date_of_birth}, ' \
               f'Email: {self.e_mail}, ' \
               f'Education: {self.education}, ' \
               f'Experience: {self.exp_years}, ' \
               f'Groups: {self.groups}'

    @classmethod
    def generate_teachers(cls, count):
        fake = Faker()
        for _ in range(count):
            teacher = generate_teacher(fake)
            obj = cls(
                      first_name=teacher['first_name'],
                      last_name=teacher['last_name'],
                      age=teacher['age'],
                      date_of_birth=teacher['date_of_birth'],
                      e_mail=teacher['e_mail'],
                      education=teacher['education'],
                      exp_years=teacher['exp_years'],
                      groups=teacher['groups']
                     )
            obj.save()
