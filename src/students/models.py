"""
The module is a realization for students of groups in LMS.

"""
import random

from django.db import models
#
from faker import Faker

# Create your models here.


class Student(models.Model):
    """
    The class contains class attributes only
    and covers personal information on a student of a group.
    """

    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=90, null=False)
    age = models.IntegerField(null=True, default=21)

    def __str__(self):
        return f'Name: {self.first_name} {self.last_name}, Age: {self.age}'

    @classmethod
    def generate_students(cls, count):
        faker = Faker()

        for _ in range(count):
            obj = cls(
                      first_name=faker.first_name(),
                      last_name=faker.last_name(),
                      age=random.randint(16, 60)
                     )
            obj.save()
