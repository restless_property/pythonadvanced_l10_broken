from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render  # noqa
from django.views.decorators.csrf import csrf_exempt
#
from students.forms import StudentCreateForm
from students.models import Student
from students.tools import format_table

# Create your views here.


def get_students(request):
    students = Student.objects.all().order_by('-id')

    # --- Substitutes ---
    params = ['first_name',
              'first_name__startswith',
              'first_name__endswith',
              'last_name',
              'age',
              'age__gt']

    # for param_name in params:
    #     param_value = request.GET.get(param_name)
    #     if param_value:
    #         students = students.filter(**{param_name: param_value})

    for param_name in params:
        param_value = request.GET.get(param_name)
        if param_value:
            param_elems = param_value.split(',')
            if param_elems:
                or_filter = Q()
                for param_elem in param_elems:
                    or_filter |= Q(**{param_name: param_elem})
                students = students.filter(or_filter)
            else:
                students = students.filter(**{param_name: param_value})
    # ---

    # --- Substituted ---
    # first_name = request.GET.get('first_name')
    # if first_name:
    #     students = students.filter(first_name=first_name)
    # last_name = request.GET.get('last_name')
    # if last_name:
    #     students = students.filter(last_name=last_name)
    # age = request.GET.get('age')
    # if age:
    #     students = students.filter(age=age)
    # ---
    form = """
            <form>
              <label >First name:</label><br>
              <input type="text" name="first_name" placeholder="Enter your first name"><br>
    
              <label >Last name:</label><br>
              <input type="text" name="last_name" placeholder="Enter your last name"><br>
    
              <label >Age:</label><br>
              <input type="number" name="age" placeholder="Enter your age"><br><br>
    
              <input type="submit" value="Submit">
            </form> 
            """

    result = format_table(students)
    return HttpResponse(form + result)


@csrf_exempt
def create_students(request):
    if request.method == 'POST':
        form = StudentCreateForm(request.POST)
        if form.isvalid():
            form.save()
            return HttpResponseRedirect('/students/')

        # first_name = request.GET.get('first_name')
        # last_name = request.GET.get('last_name')
        # age = int(request.GET.get('age'))
        #
        # student = Student(first_name=first_name,
        #                   last_name=last_name,
        #                   age=age)
        # student.save()
        # return HttpResponseRedirect('/students/')

    elif request.method == 'GET':

        form = StudentCreateForm()
    html_template = """
                            <form method='post'>
                              {}
                              <input type="submit" value="Create students">
                            </form> 
                            """
    result = html_template.format(form.as_p())
    return HttpResponse(result)
